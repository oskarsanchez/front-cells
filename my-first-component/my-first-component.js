class MyFirstComponent extends Polymer.Element {

  static get is() {
    return 'my-first-component';
  }

  static get properties() {
    return {
      title : {
        type : String,
        value: ''
      },
      alternateTitle: {
        type : String,
        value: ''
      }
    };

  }
  _handleAlternateTitleClick(){
    this.alternateTitle = "Evento manejado con el click";
  }
  _handleMessageSend(){
    this.alternateTitle = " titulo cambiado por el handleMessageSend";

  }
}

customElements.define(MyFirstComponent.is, MyFirstComponent);
