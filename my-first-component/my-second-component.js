class MySecondComponent extends Polymer.Element {

  static get is() {
    return 'my-second-component';
  }

  static get properties() {
    return {
      title : String,
      notify : true
    };

  }
  _handleClick(){
    this.title = "titulo cambiado por click en second componente";
    this.dispatchEvent(new CustomEvent('message-send',{bubbles:true, composed:true}));    
  }
}

customElements.define(MySecondComponent.is, MySecondComponent);
